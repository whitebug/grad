import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:grad/resources/strings.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:grad/web/partnerAPI.dart' as partnerAPI;
import 'package:grad/web/helpAPI.dart' as helpAPI;
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';


class AlertUtil {

  static AlertStyle createCustomAlertStyle() {
    return AlertStyle(
      animationType: AnimationType.fromTop,
      isCloseButton: false,
      isOverlayTapDismiss: false,
      titleStyle: TextStyle(
        color: Colors.black,
        fontSize: 18,
        fontFamily: "Gotham",
        fontWeight: FontWeight.w200
      ),
      //buttonAreaPadding: EdgeInsets.all(0.0)
    );
  }

  static void createQrSuccessAlert(BuildContext context, Function callback) {
    Alert(
      context: context,
      title: Strings.alertQrSuccessTitle,
      style: createCustomAlertStyle(),
      buttons: [
        DialogButton(
          onPressed: () => callback(), //Navigator.pop(context),
          child: Text(
            Strings.alertQrSuccessButton,
            style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: "Gotham",
                fontWeight: FontWeight.w200),
          ),
        ),
      ],
    ).show();
  }

  static void createCustomAlert(BuildContext context, String text) {
    Alert(
      context: context,
      title: text,
      style: createCustomAlertStyle(),
      buttons: [
        DialogButton(
          onPressed: () => Navigator.pop(context),
          child: Text(
            Strings.customAlertOk,
            style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: "Gotham",
                fontWeight: FontWeight.w200),
          ),
        ),
      ],
    ).show();
  }

  static void createCustomAlertWithCallback(
      BuildContext context, String text, Function callback) {
    Alert(
      context: context,
      title: text,
      style: createCustomAlertStyle(),
      buttons: [
        DialogButton(
          onPressed: () {
            Navigator.pop(context);
            callback();
          },
          child: Text(
            Strings.customAlertOk,
            style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: "Gotham",
                fontWeight: FontWeight.w200),
          ),
        ),
      ],
    ).show();
  }

  static void createErrorAlert(BuildContext context) {
    Alert(
      context: context,
      title: Strings.alertErrorTitle,
      style: createCustomAlertStyle(),
      buttons: [
        DialogButton(
          onPressed: () => Navigator.pop(context),
          child: Text(
            Strings.alertErrorButton,
            style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: "Gotham",
                fontWeight: FontWeight.w200),
          ),
        ),
      ],
    ).show();
  }

  static void createErrorAlertWithCallback(
      BuildContext context, Function callback) {
    Alert(
      context: context,
      title: Strings.alertErrorTitle,
      style: createCustomAlertStyle(),
      buttons: [
        DialogButton(
          onPressed: () {
            Navigator.pop(context);
            callback();
          },
          child: Text(
            Strings.alertErrorButton,
            style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: "Gotham",
                fontWeight: FontWeight.w200),
          ),
        ),
      ],
    ).show();
  }

  static void createTechHelpAlert(BuildContext context, String savedPhone ) {

    String name;
    String phone = savedPhone  ?? "+7";
    String message;

    var buttonEnabled = false;

    enableButton() {
      buttonEnabled = name != null &&
          name.isNotEmpty &&
          phone != null &&
          phone.isNotEmpty &&
          message != null &&
          message.isNotEmpty;
    }

    var controller =
        new MaskedTextController(mask: '+70000000000', text: savedPhone );
    if (savedPhone != null && savedPhone.isNotEmpty) {
      controller.updateText(savedPhone);
    }

    void _makeHelpDescRequest(
        String name, String phone, String message, BuildContext context) async {
      ProgressDialog loader = AlertUtil.getLoader(context);
      loader.show();
      http
          .post(helpAPI.getHelpDescUrl(),
              body: helpAPI.formHelpDescMap(name, phone, message))
          .then((response) async {
        loader.hide().then((onValue) {
          try {
            if (response.statusCode == 204 || response.statusCode == 200) {
              AlertUtil.createCustomAlert(context, Strings.alertTechHelpSucces);
            } else {
              AlertUtil.createErrorAlert(context);
            }
          } catch (error) {
            AlertUtil.createErrorAlert(context);
          }
        });
      }).catchError((error) {
        loader.hide().then((onValue) {
          AlertUtil.createErrorAlert(context);
        });
      });
    }

    Alert(
      context: context,
      title: Strings.alertTechHelpTitle,
      style: createCustomAlertStyle(),
      content: Column(
        children: <Widget>[
          Text(
            Strings.alertTechHelpSubtitle,
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: "Gotham",
                fontWeight: FontWeight.w200
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 16.0),
            child: TextField(
              controller: controller,
//              keyboardType: TextInputType.phone,
              textAlign: TextAlign.center,
              onChanged: ((text){
                if (text.length > 12)
                  phone = text.substring(0, 12);
                else
                  phone = text;

                enableButton();
              }),
              style: TextStyle(
                decoration: TextDecoration.none,
                fontFamily: "Gotham",
                fontWeight: FontWeight.w200,
                color: Colors.black,
              ),
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey),
                    borderRadius: BorderRadius.circular(0.0)
                ),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey),
                    borderRadius: BorderRadius.circular(0.0)
                ),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey),
                    borderRadius: BorderRadius.circular(0.0)
                ),
                hintText: Strings.alertTechHelpHintPhone,
                hintStyle: TextStyle(
                  color: Colors.grey,
                ),
              ),
            )
          ),
          Padding(
              padding: EdgeInsets.only(top: 16.0),
              child: TextField(
                textAlign: TextAlign.center,
                onChanged: ((text){
                  name = text;
                  enableButton();
                }),
                style: TextStyle(
                  decoration: TextDecoration.none,
                  fontFamily: "Gotham",
                  fontWeight: FontWeight.w200,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(0.0)
                  ),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(0.0)
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(0.0)
                  ),
                  hintText: Strings.alertTechHelpHintName,
                  hintStyle: TextStyle(
                    color: Colors.grey,
                  ),
                ),
              )
          ),
          Padding(
              padding: EdgeInsets.only(top: 16.0),
              child: TextField(
                textAlign: TextAlign.center,
                onChanged: ((text){
                  message = text;
                  enableButton();
                }),
                style: TextStyle(
                  decoration: TextDecoration.none,
                  fontFamily: "Gotham",
                  fontWeight: FontWeight.w200,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(0.0)
                  ),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(0.0)
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(0.0)
                  ),
                  hintText: Strings.alertTechHelpHint,
                  hintStyle: TextStyle(
                    color: Colors.grey,
                  ),
                ),
              )
          ),
        ],
      ),
      buttons: [
        DialogButton(
          onPressed: () => Navigator.pop(context),
          child: Text(
            Strings.alertTechHelpButtonClose,
            style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: "Gotham",
                fontWeight: FontWeight.w200
            ),
          ),
        ),
        DialogButton(
          onPressed: ((){
            if (buttonEnabled)
              _makeHelpDescRequest(name, phone, message, context);
          }),
          child: Text(
            Strings.alertTechHelpButtonOk,
            style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: "Gotham",
                fontWeight: FontWeight.w200
            ),
          ),
        ),
      ],
    ).show();
  }

  static void createPartnerRequestAlert(BuildContext context) {
    String name;
    String phone;

    var buttonEnabled = false;

    enableButton() {
      buttonEnabled = name != null && name.isNotEmpty
          && phone != null && phone.isNotEmpty;
    }

    void _makeBecameAPartnerRequest(String name, String phone, BuildContext context) async {
      ProgressDialog loader = AlertUtil.getLoader(context);
      loader.show();

      http
          .post(partnerAPI.getBecomeAPartnerUrl(),
          body: partnerAPI.formBecomeAPartnerMap(name, phone))
          .then((response) async {
          Navigator.pop(context);
            try {
              if(response.statusCode == 204 || response.statusCode == 200){
                Navigator.pop(context);
                    AlertUtil.createCustomAlert(context, Strings.alertSuccesFullPartnerRequset);
                    } else {
                Navigator.pop(context);
                      AlertUtil.createErrorAlert(context);
                    }
                  } catch (error) {
              Navigator.pop(context);
                AlertUtil.createErrorAlert(context);
              }
      }).catchError((error) {
        Navigator.pop(context);
        AlertUtil.createErrorAlert(context);
      });
    }

    Alert(
      context: context,
      title: Strings.alertPartnerRequestTitle,
      style: createCustomAlertStyle(),
      content: Column(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(top: 16.0),
              child: TextField(
                onChanged: ((text) {
                  name = text;
                  enableButton();
                }),
                style: TextStyle(
                  decoration: TextDecoration.none,
                  fontFamily: "Gotham",
                  fontWeight: FontWeight.w200,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(0.0)
                  ),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(0.0)
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(0.0)
                  ),
                  hintText: Strings.alertPartnerRequestHintName,
                  hintStyle: TextStyle(
                    color: Colors.grey,
                  ),
                ),
              )
          ),
          Padding(
              padding: EdgeInsets.only(top: 16.0),
              child: TextField(
                onChanged: ((text) {
                  phone = text;
                  enableButton();
                }),
                style: TextStyle(
                  decoration: TextDecoration.none,
                  fontFamily: "Gotham",
                  fontWeight: FontWeight.w200,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(0.0)
                  ),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(0.0)
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(0.0)
                  ),
                  hintText: Strings.alertPartnerRequestHintPhone,
                  hintStyle: TextStyle(
                    color: Colors.grey,
                  ),
                ),
              )
          ),
        ],
      ),
      buttons: [
        DialogButton(
          onPressed: () => Navigator.pop(context),
          child: Text(
            Strings.alertPartnerRequestButtonClose,
            style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: "Gotham",
                fontWeight: FontWeight.w200
            ),
          ),
        ),
        DialogButton(
          onPressed: (() {
            if (buttonEnabled)
              _makeBecameAPartnerRequest(name, phone, context);
          }),
          child: Text(
            Strings.alertPartnerRequestButtonSend,
            style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: "Gotham",
                fontWeight: FontWeight.w200
            ),
          ),
        ),
      ],
    ).show();
  }

  static ProgressDialog getLoader (BuildContext context) {
    var pb = new ProgressDialog(context, isDismissible: false, type: ProgressDialogType.Normal);
    pb.style(message: Strings.loaderTilte);
    return pb;
  }
}
