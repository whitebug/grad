import 'package:flutter/material.dart';
import 'package:grad/web/categoriesAPI.dart';

class CardUtil {
  static String categories({@required List<CategoryModel> categories, @required String defaultText}) {
    if (categories == null) return defaultText;
    List<String> categoryStrings = List();
    for (int i = 0; i < categories.length; i++) {
      categoryStrings.add(categories[i].name);
    }
    return categoryStrings.join(', ');
  }
}
