

const ROUTE_MAIN_AUTH = "route_main_auth";
const ROUTE_USER_SMS = "route_user_sms";
const ROUTE_USER_HOME = "route_user_home";
const ROUTE_USER_AGREEMENT = "route_user_agreement";
const ROUTE_PARTNER_CODE = "route_partner_code";
const ROUTE_PARTNER_HOME = "route_partner_home";
const ROUTE_DEVELOPERS = "route_developers";
const ROUTE_ABOUT_APP = "route_about_app";
const ROUTE_START = "route_start";
