import 'package:flutter/material.dart';
import 'package:grad/components/text.dart';
import 'package:grad/screens/about_app.dart';
import 'package:grad/screens/auth.dart';
import 'package:grad/screens/auth_partner.dart';
import 'package:grad/screens/auth_user.dart';
import 'package:grad/screens/developers.dart';
import 'package:grad/screens/home_partner.dart';
import 'package:grad/screens/home_user.dart';
import 'package:grad/screens/start_screen.dart';
import 'package:grad/screens/user_agreement.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:grad/cache/SharedPrefsHelper.dart' as cache;
import 'package:grad/resources/strings.dart';
import 'package:grad/Routes.dart' as router;

void main() => runApp(MyApp());

const MaterialColor white = const MaterialColor(
  0xFFFFFFFF,
  const <int, Color>{
    50: const Color(0xFFFFFFFF),
    100: const Color(0xFFFFFFFF),
    200: const Color(0xFFFFFFFF),
    300: const Color(0xFFFFFFFF),
    400: const Color(0xFFFFFFFF),
    500: const Color(0xFFFFFFFF),
    600: const Color(0xFFFFFFFF),
    700: const Color(0xFFFFFFFF),
    800: const Color(0xFFFFFFFF),
    900: const Color(0xFFFFFFFF),
  },
);

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: Strings.appName,
        routes: {
          router.ROUTE_MAIN_AUTH : (context) => AuthScreen(),
          router.ROUTE_USER_SMS : (context) => AuthUserScreen(),
          router.ROUTE_USER_HOME : (context) => HomeUserScreen(),
          router.ROUTE_PARTNER_CODE : (context) => AuthPartnerScreen(),
          router.ROUTE_PARTNER_HOME : (context) => HomePartnerScreen(),
          router.ROUTE_USER_AGREEMENT : (context) => UserAgreementPage(),
          router.ROUTE_DEVELOPERS: (context) => DevelopersScreen(),
          router.ROUTE_START: (context) => StartScreen(),
          router.ROUTE_ABOUT_APP: (context) => AboutAppScreen(),
        },
        theme: ThemeData(
          primarySwatch: white,
          unselectedWidgetColor: white,
          toggleableActiveColor: Colors.transparent,
        ),
        home: SplashScreen());
  }
}

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    loadData();
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 5), onDoneLoading);
  }

  onDoneLoading() async {

    final prefs = await SharedPreferences.getInstance();
    cache.Roles role = await cache.getRole(prefs);
      switch (role) {
        case cache.Roles.firstLaunch:
          {
            Navigator.pushReplacementNamed(context, router.ROUTE_START);
            return;
          }
        case cache.Roles.notAuth:
          {
            Navigator.pushReplacementNamed(context, router.ROUTE_MAIN_AUTH);
            return;
          }

        case cache.Roles.authUser:
          {
            Navigator.pushReplacementNamed(context, router.ROUTE_USER_HOME);
            return;
          }

        case cache.Roles.authPartner:
          {
            Navigator.pushReplacementNamed(context, router.ROUTE_PARTNER_HOME);
            return;
          }
      }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/images/gradient_background.png'),
            fit: BoxFit.cover),
      ),
      child: Center(
        child: Padding(
          padding: EdgeInsets.all(32.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Image(
                image: Image(image: AssetImage('assets/images/logo.png')).image,
              ),
              CustomText(
                textTitle: Strings.splashText,
                textSize: 20,
                textWeight: FontWeight.w200,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
