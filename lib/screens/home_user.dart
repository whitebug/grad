import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:grad/Routes.dart' as router;
import 'package:grad/cache/SharedPrefsHelper.dart' as cache;
import 'package:grad/components/flat_button.dart';
import 'package:grad/components/pdf_viewer.dart';
import 'package:grad/resources/urls.dart';
import 'package:grad/screens/pages/categories_page.dart';
import 'package:grad/screens/pages/partner_page.dart';
import 'package:grad/screens/pages/partners_page.dart';
import 'package:grad/screens/pages/qr_code_page.dart';
import 'package:grad/utils/alert_util.dart';
import 'package:grad/utils/url_opener_util.dart';
import 'package:grad/web/categoriesAPI.dart';
import 'package:grad/web/userApi.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../components/text.dart';
import '../resources/strings.dart';

class HomeUserScreen extends StatefulWidget {
  @override
  HomeUserScreenState createState() => HomeUserScreenState();
}

class HomeUserScreenState extends State<HomeUserScreen> {
  final PageStorageBucket bucket = PageStorageBucket();

  String title = '';
  String userId;
  String phone;
  int _selectedIndex = 0;
  int _selectedCategoryId = 1;
  PartnerModel partnerModel;

  Widget _getPageByIndex() {
    switch (_selectedIndex) {
      case 0:
        return QrCodePage(
          key: PageStorageKey('qr page'),
          userId: userId,
        );
      case 1:
        return CategoriesPage(
          key: PageStorageKey('categories page'),
          onCategoryClick: (id) {
            _selectedCategoryId = id;
            _onItemTapped(2);
          },
        );
      case 2:
        return PartnersPage(
          key: PageStorageKey('partners page'),
          categoryId: _selectedCategoryId,
          onPartnerClick: (partnerModel) {
            this.partnerModel = partnerModel;
            _onItemTapped(3);
          },
        );
      default:
        return PartnerPage(
          model: partnerModel,
          key: PageStorageKey('partner page'),
        );
    }
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    Future f = _getUserId();
    f.then((value) {
      setState(() {
        title = 'ID$value';
        userId = value.toString();
      });
    });

    Future phoneFuture = _getUserPhone();
    phoneFuture.then((value) {
      setState(() {
        phone = value.toString();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (_selectedIndex == 3) {
          _onItemTapped(2);
          return false;
        } else {
          return true;
        }
      },
      child: Scaffold(
        appBar: AppBar(
          iconTheme:
              _selectedIndex != 3 ? IconThemeData(color: Colors.white) : null,
          leading: _selectedIndex == 3
              ? IconButton(
                  icon: Icon(
                    Icons.keyboard_arrow_left,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    _onItemTapped(2);
                  },
                )
              : null,
          title: CustomText(
            textTitle: _selectedIndex == 0
                ? title
                : _selectedIndex == 1
                    ? "Категории партнеров"
                    : _selectedIndex == 2 ? "Список партнеров" : "Партнер",
            textWeight: FontWeight.w300,
            textSize: 18,
          ),
          flexibleSpace: Image(
            image: AssetImage('assets/images/gradient_background.png'),
            fit: BoxFit.cover,
          ),
          backgroundColor: Colors.transparent,
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/gradient_background.png'),
                fit: BoxFit.cover),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(
                child: PageStorage(
                  child: _getPageByIndex(),
                  bucket: bucket,
                ),
              ),
              Container(
                height: 1,
                color: Color(0xFF074753),
              ),
              Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    stops: [0.0, 0.34, 0.66, 0.92, 1.0],
                    colors: [
                      Color(0xFF002027),
                      Color(0xDF01262E),
                      Color(0x8A05373F),
                      Color(0x4F07424C),
                      Color(0x000A525C),
                    ],
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () => _onItemTapped(0),
                        child: Container(
                          color: Colors.transparent,
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 11),
                              Image.asset(
                                'assets/images/ic_qr_code.png',
                                width: 32,
                                height: 32,
                                color: _selectedIndex == 0
                                    ? Colors.white
                                    : Color(0xFF85ACB5),
                              ),
                              SizedBox(height: 3),
                              Text(
                                Strings.bottomMenuQrCode,
                                style: TextStyle(
                                  decoration: TextDecoration.none,
                                  fontFamily: "Gotham",
                                  fontWeight: FontWeight.w200,
                                  fontSize: 13,
                                  color: _selectedIndex == 0
                                      ? Colors.white
                                      : Color(0xFF85ACB5),
                                ),
                              ),
                              SizedBox(height: 5),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () => _onItemTapped(1),
                        child: Container(
                          color: Colors.transparent,
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 11),
                              Image.asset(
                                'assets/images/ic_category.png',
                                width: 32,
                                height: 32,
                                color: _selectedIndex == 1
                                    ? Colors.white
                                    : Color(0xFF85ACB5),
                              ),
                              SizedBox(height: 3),
                              Text(
                                Strings.bottomMenuCategories,
                                style: TextStyle(
                                  decoration: TextDecoration.none,
                                  fontFamily: "Gotham",
                                  fontWeight: FontWeight.w200,
                                  fontSize: 13,
                                  color: _selectedIndex == 1
                                      ? Colors.white
                                      : Color(0xFF85ACB5),
                                ),
                              ),
                              SizedBox(height: 5),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          _selectedCategoryId = 0;
                          _onItemTapped(2);
                        },
                        child: Container(
                          color: Colors.transparent,
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              SizedBox(height: 11),
                              Image.asset(
                                'assets/images/ic_partners_2.png',
                                width: 32,
                                height: 32,
                                color:
                                    _selectedIndex == 2 || _selectedIndex == 3
                                        ? Colors.white
                                        : Color(0xFF85ACB5),
                              ),
                              SizedBox(height: 3),
                              Text(
                                Strings.bottomMenuPartners,
                                style: TextStyle(
                                  decoration: TextDecoration.none,
                                  fontFamily: "Gotham",
                                  fontWeight: FontWeight.w200,
                                  fontSize: 13,
                                  color:
                                      _selectedIndex == 2 || _selectedIndex == 3
                                          ? Colors.white
                                          : Color(0xFF85ACB5),
                                ),
                              ),
                              SizedBox(height: 5),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        drawer: Drawer(
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/gradient_background.png'),
                  fit: BoxFit.cover),
            ),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: ListView(
                    // Important: Remove any padding from the ListView.
                    padding: EdgeInsets.zero,
                    children: <Widget>[
                      SizedBox(
                        height: 48.0,
                      ),
//          git merge          ListTile(
//                      title: CustomText(
//                        textTitle: Strings.menuMain,
//                        textWeight: FontWeight.w200,
//                        textSize: 16,
//                        textAlign: TextAlign.start,
//                      ),
//                      leading: Image.asset(
//                        'assets/images/ic_main.png',
//                        width: 32,
//                        height: 32
//                      ),
//                      onTap: () {

//                      },
//                    ),
                      ListTile(
                        title: CustomText(
                          textTitle: Strings.menuMain,
                          textWeight: FontWeight.w200,
                          textSize: 16,
                          textAlign: TextAlign.start,
                        ),
                        leading: Image.asset('assets/images/ic_main.png',
                            width: 32, height: 32),
                        onTap: () {
                          _onItemTapped(0);
                          Navigator.pop(context);
                        },
                      ),
                      ListTile(
                        title: CustomText(
                          textTitle: Strings.menuTechHelp,
                          textWeight: FontWeight.w200,
                          textSize: 16,
                          textAlign: TextAlign.start,
                        ),
                        leading: Image.asset('assets/images/ic_tech_help.png',
                            width: 32, height: 32),
                        onTap: () {
                          Navigator.pop(context);
                          AlertUtil.createTechHelpAlert(context, phone);
                        },
                      ),
                      ListTile(
                        title: CustomText(
                          textTitle: Strings.menuRules,
                          textWeight: FontWeight.w200,
                          textSize: 16,
                          textAlign: TextAlign.start,
                        ),
                        leading: Image.asset('assets/images/ic_rules.png',
                            width: 32, height: 32),
                        onTap: () {
                          Navigator.pop(context);
                          // UrlOpenerUtil.launchURL(Urls.urlRules);
                          preparePdf(pdf: 'assets/pdf/rules.pdf').then((path) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PdfViewer(path)),
                            );
                          });
                        },
                      ),
                      ListTile(
                        title: CustomText(
                          textTitle: Strings.menuOffer,
                          textWeight: FontWeight.w200,
                          textSize: 16,
                          textAlign: TextAlign.start,
                        ),
                        leading: Image.asset('assets/images/ic_offer.png',
                            width: 32, height: 32),
                        onTap: () {
                          Navigator.pop(context);
                          // UrlOpenerUtil.launchURL(Urls.urlOffer);
                          preparePdf(pdf: 'assets/pdf/offer.pdf').then((path) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PdfViewer(path)),
                            );
                          });
                        },
                      ),
                      ListTile(
                        title: CustomText(
                          textTitle: Strings.menuAboutApp,
                          textWeight: FontWeight.w200,
                          textSize: 16,
                          textAlign: TextAlign.start,
                        ),
                        leading: Image.asset('assets/images/ic_about_app.png',
                            width: 32, height: 32),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.pushNamed(context, router.ROUTE_ABOUT_APP);
                        },
                      ),
                      ListTile(
                        title: CustomText(
                          textTitle: Strings.menuInstagram,
                          textWeight: FontWeight.w200,
                          textSize: 16,
                          textAlign: TextAlign.start,
                        ),
                        leading: Image.asset('assets/images/ic_instagram.png',
                            width: 32, height: 32),
                        onTap: () {
                          Navigator.pop(context);
                          UrlOpenerUtil.launchURL(Urls.urlInstagram);
                        },
                      ),
                    ],
                  ),
                ),
                CustomFlatButton(
                  titleText: Strings.developersTitle,
                  textWeight: FontWeight.w200,
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, router.ROUTE_DEVELOPERS);
                  },
                ),
                SizedBox(
                  height: 24.0,
                ),
                CustomFlatButton(
                  titleText: Strings.menuQuit,
                  textWeight: FontWeight.w200,
                  onPressed: () {
                    _removeUserFromCache();
                  },
                ),
                SizedBox(
                  height: 24.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<String> preparePdf({@required String pdf}) async {
    final ByteData bytes =
    await DefaultAssetBundle.of(context).load(pdf);
    final Uint8List list = bytes.buffer.asUint8List();

    final tempDir = await getTemporaryDirectory();
    final tempDocumentPath = '${tempDir.path}/$pdf';

    final file = await File(tempDocumentPath).create(recursive: true);
    file.writeAsBytesSync(list);
    return tempDocumentPath;
  }

  Future<int> _getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    UserInfo userInfo = cache.getUserInfo(prefs);
    return userInfo.customerId;
  }

  Future<void> _removeUserFromCache() async {
    final prefs = await SharedPreferences.getInstance();
    cache.setRole(prefs, cache.Roles.notAuth);
    cache.removeUserInfo(prefs);
    Navigator.pushNamedAndRemoveUntil(
        context, router.ROUTE_MAIN_AUTH, (Route<dynamic> route) => false);
    return;
  }

  Future<String> _getUserPhone() async {
    final prefs = await SharedPreferences.getInstance();
    String phone = cache.getPhone(prefs);
    return phone;
  }
}
