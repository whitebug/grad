import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grad/cache/SharedPrefsHelper.dart' as cache;
import 'package:grad/components/partners_item.dart';
import 'package:grad/utils/alert_util.dart';
import 'package:grad/web/categoriesAPI.dart' as categoriesAPI;
import 'package:grad/web/categoriesAPI.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PartnersPage extends StatefulWidget {
  final Function(PartnerModel) onPartnerClick;
  final int categoryId;

  PartnersPage({
    Key key,
    @required this.onPartnerClick,
    @required this.categoryId,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PartnersPageState();
  }
}

class _PartnersPageState extends State<PartnersPage> {
  List<PartnerModel> partners = [];

  void _loadPartners(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    var partner = cache.getPartnerInfo(prefs);
    print(widget.categoryId.toString());
    await get(categoriesAPI.getCategoryUrl(widget.categoryId),
            headers: categoriesAPI.formHeaderPartners(partner.token, 1))
        .then((response) {
      var decodedJson = jsonDecode(utf8.decode(response.bodyBytes));

      categoriesAPI.PartnersResponse partnersResponse =
          categoriesAPI.PartnersResponse.fromJson(decodedJson);
      if (partnersResponse.success) {
        setState(() {
          this.partners = partnersResponse.partners;
        });
      } else {
        print(partnersResponse.message);
        AlertUtil.createErrorAlertWithCallback(context, () {
          _loadPartners(context);
        });
      }
    }).catchError((onError) {
      print(onError.toString());
      AlertUtil.createErrorAlertWithCallback(context, () {
        _loadPartners(context);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _loadPartners(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: SingleChildScrollView(
        child: Column(
          children: partners
              .map((partnerModel) => PartnersItem(
                    model: partnerModel,
                    onClick: (model) {
                      widget.onPartnerClick(model);
                    },
                  ))
              .toList(),
        ),
      ),
    );
  }
}
