import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grad/cache/SharedPrefsHelper.dart' as cache;
import 'package:grad/components/categories_item.dart';
import 'package:grad/utils/alert_util.dart';
import 'package:grad/web/categoriesAPI.dart' as categoriesAPI;
import 'package:grad/web/categoriesAPI.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CategoriesPage extends StatefulWidget {
  final Function(int) onCategoryClick;

  CategoriesPage({Key key, this.onCategoryClick}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _CategoriesPageState();
  }
}

class _CategoriesPageState extends State<CategoriesPage> {
  List<CategoryModel> categories = [];

  void _loadCategories(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    var partner = cache.getPartnerInfo(prefs);

    await get(categoriesAPI.getCategoriesUrl(),
            headers: categoriesAPI.formHeader(partner.token))
        .then((response) {
      var decodedJson = jsonDecode(utf8.decode(response.bodyBytes));
      categoriesAPI.CategoriesResponse categoriesResponse =
          categoriesAPI.CategoriesResponse.fromJson(decodedJson);
      if (categoriesResponse.success) {
        setState(() {
          categories = categoriesResponse.categories;
        });
      } else {
        AlertUtil.createErrorAlertWithCallback(context, () {
          _loadCategories(context);
        });
      }
    }).catchError((onError) {
      AlertUtil.createErrorAlertWithCallback(context, () {
        _loadCategories(context);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _loadCategories(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: SingleChildScrollView(
        child: Align(
          alignment: Alignment.topLeft,
          child: Wrap(
            children: categories
                .map(
                  (category) => SizedBox(
                    width: MediaQuery.of(context).size.width / 3,
                    child: CategoriesItem(
                      text: category.name,
                      image: category.imgLink,
                      alignment: Alignment.center,
                      onClick: () {
                        widget.onCategoryClick(category.id);
                      },
                    ),
                  ),
                )
                .toList(),
          ),
        ),
      ),
    );
  }
}
