import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grad/components/text.dart';
import 'package:grad/resources/strings.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QrCodePage extends StatefulWidget {
  final String userId;
  QrCodePage({Key key, @required this.userId}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _QrCodeState();
  }
}

class _QrCodeState extends State<QrCodePage> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              child: QrImage(
                data: widget.userId,
                version: QrVersions.auto,
                backgroundColor: Colors.white,
                size: 200.0,
              ),
              padding: EdgeInsets.only(top: 0),
            ),
            SizedBox(height: 40),
            Padding(
              padding: EdgeInsets.only(left: 35, right: 35),
              child: CustomText(
                textTitle: Strings.userHomeShowYourQR,
                textSize: 16,
                textWeight: FontWeight.w300,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
