import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:grad/utils/cards_util.dart';
import 'package:grad/utils/url_opener_util.dart';
import 'package:grad/web/categoriesAPI.dart';
import 'package:grad/web/webApi.dart';

class PartnerPage extends StatelessWidget {
  final PartnerModel model;

  PartnerPage({Key key, @required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 10, left: 38, right: 38, bottom: 20),
          child: Column(
            children: <Widget>[
              Text(
                CardUtil.categories(
                  categories: model.category,
                  defaultText: '{название категории}',
                )
                /*model.category == null
                    ? "{название категории}"
                    : model.category.name*/
                ,
                style: TextStyle(
                  decoration: TextDecoration.none,
                  fontFamily: "Gotham",
                  fontWeight: FontWeight.w100,
                  fontSize: 14,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 10),
              Image.network(
                BASE_URL_SHORT + model.img,
                fit: BoxFit.cover,
                height: 150,
              ),
              SizedBox(height: 10),
              Text(
                model.name,
                style: TextStyle(
                  decoration: TextDecoration.none,
                  fontFamily: "Gotham",
                  fontWeight: FontWeight.w300,
                  fontSize: 18,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 15),
              Container(
                color: Colors.white,
                height: 1,
                margin: EdgeInsets.only(left: 20, right: 20),
              ),
              SizedBox(height: 8),
              Text(
                model.descFull == null ? "{полное описание}" : model.descFull,
                textAlign: TextAlign.justify,
                style: TextStyle(
                  decoration: TextDecoration.none,
                  fontFamily: "Gotham",
                  fontWeight: FontWeight.w300,
                  fontSize: 13,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 8),
              Container(
                color: Colors.white,
                height: 1,
                margin: EdgeInsets.only(left: 20, right: 20),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Предоставляемая скидка: ${model.sale}%',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        decoration: TextDecoration.none,
                        fontFamily: "Gotham",
                        fontWeight: FontWeight.w300,
                        fontSize: 13,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                color: Colors.white,
                height: 1,
                margin: EdgeInsets.only(left: 20, right: 20),
              ),
              SizedBox(height: 20),
              Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.centerLeft,
                          child: RichText(
                            text: TextSpan(
                                text: "Сайт: ",
                                style: TextStyle(
                                  decoration: TextDecoration.none,
                                  fontFamily: "Gotham",
                                  fontWeight: FontWeight.w300,
                                  fontSize: 13,
                                  color: Colors.white,
                                ),
                                children: [
                                  TextSpan(
                                    text: model.link == null
                                        ? "{ link }"
                                        : model.link,
                                    style: TextStyle(
                                      decoration: TextDecoration.underline,
                                      fontFamily: "Gotham",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 13,
                                      color: Colors.white,
                                    ),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        UrlOpenerUtil.launchURL(model.link);
                                      },
                                  )
                                ]),
                          ),
                        ),
                        SizedBox(height: 8),
                        Column(
                          children: model.phones
                              .map(
                                (phone) => Align(
                                  alignment: Alignment.centerLeft,
                                  child: GestureDetector(
                                    onTap: () {
                                      UrlOpenerUtil.launchURL(
                                          "tel:" + phone.phone);
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.only(bottom: 8),
                                      child: Text(
                                        phone.phone,
                                        style: TextStyle(
                                          decoration: TextDecoration.underline,
                                          fontFamily: "Gotham",
                                          fontWeight: FontWeight.w400,
                                          fontSize: 13,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                              .toList(),
                        ),
                        SizedBox(height: 8),
                      ],
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Wrap(
                        children: <Widget>[
                          model.instagram != null
                              ? GestureDetector(
                                  onTap: () {
                                    UrlOpenerUtil.launchURL(model.instagram);
                                  },
                                  child: Image.asset(
                                    'assets/images/ic_partner_instagram.png',
                                    height: 50,
                                  ),
                                )
                              : SizedBox(),
                          model.vk != null
                              ? GestureDetector(
                                  onTap: () {
                                    UrlOpenerUtil.launchURL(model.vk);
                                  },
                                  child: Image.asset(
                                    'assets/images/ic_partner_vk.png',
                                    height: 50,
                                  ),
                                )
                              : SizedBox(),
                          model.facebook != null
                              ? GestureDetector(
                                  onTap: () {
                                    UrlOpenerUtil.launchURL(model.facebook);
                                  },
                                  child: Image.asset(
                                    'assets/images/ic_partner_facebook.png',
                                    height: 50,
                                  ),
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Адрес:",
                  style: TextStyle(
                    fontFamily: "Gotham",
                    fontWeight: FontWeight.w300,
                    fontSize: 13,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(height: 14),
              Column(
                children: model.addresses
                    .map(
                      (address) => GestureDetector(
                        onTap: () {
                          if (address.latitude != null &&
                              address.longitude != null) {
                            UrlOpenerUtil.launchURL(
                                "https://www.google.com/maps/search/?api=1&query=" +
                                    address.latitude.toString() +
                                    "," +
                                    address.longitude.toString());
                          }
                        },
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 8),
                          child: Row(
                            children: <Widget>[
                              Image.asset(
                                'assets/images/ic_marker.png',
                                height: 25,
                                width: 25,
                              ),
                              Expanded(
                                child: Text(
                                  address.address,
                                  style: TextStyle(
                                    decoration: address.latitude != null &&
                                            address.longitude != null
                                        ? TextDecoration.underline
                                        : TextDecoration.none,
                                    fontFamily: "Gotham",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 13,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                    .toList(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
