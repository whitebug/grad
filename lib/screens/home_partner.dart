import 'package:flutter/material.dart';
import 'package:grad/components/flat_button.dart';
import 'package:grad/components/text.dart';
import 'package:grad/utils/alert_util.dart';
import 'package:grad/web/partnerAPI.dart';
import 'package:grad/web/qrAPI.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:qrcode/qrcode.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../resources/strings.dart';
import 'package:grad/cache/SharedPrefsHelper.dart' as cache;
import 'package:grad/Routes.dart' as router;
import 'package:http/http.dart' as http;

class HomePartnerScreen extends StatefulWidget {
  @override
  HomePartnerScreenState createState() => HomePartnerScreenState();
}

class HomePartnerScreenState extends State<HomePartnerScreen> {
  QRCaptureController _captureController = QRCaptureController();

  String capturedData = '';

  String phone;

  bool scanPaused = true;
  String scanButtonText = Strings.partnerHomeTitleStartQr;

  String title = '';

  _pauseScan(bool pause) {
    scanPaused = pause;
    scanButtonText = scanPaused
        ? Strings.partnerHomeTitleStartQr
        : Strings.partnerHomeTitleStopQr;
    if (scanPaused)
      _captureController.pause();
    else
      _captureController.resume();
  }

  @override
  void didUpdateWidget(HomePartnerScreen oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);

    _pauseScan(scanPaused);
  }

  @override
  void initState() {
    super.initState();

    _pauseScan(true);

    _captureController.onCapture((data) {
      _pauseScan(true);
      //capturedData = data;
      setState(() {});
      _makeQRRequest(data);
    });
  }

  @override
  Widget build(BuildContext context) {
    Future phoneFuture = _getUserPhone();
    phoneFuture.then((value) {
      setState(() {
        phone = value.toString();
      });
    });

    Future partnerNameFuture = _getPartnerName();
    partnerNameFuture.then((value) {
      setState(() {
        title = value;
      });
    });

    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          textTitle: title,
          textWeight: FontWeight.w200,
          textSize: 24,
        ),
        iconTheme: IconThemeData(color: Colors.white),
        flexibleSpace: Image(
          image: AssetImage('assets/images/gradient_background.png'),
          fit: BoxFit.cover,
        ),
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
      ),
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            child: Align(
              alignment: Alignment.center,
              child: QRCaptureView(controller: _captureController),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/gradient_background_qr.png'),
                fit: BoxFit.cover,
              ),
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: _setupWidgets(), //_buildToolBar(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _setupWidgets() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 48.0),
          child: CustomFlatButton(
            onPressed: () {
              _pauseScan(!scanPaused);
              setState(() {});
            },
            titleText: scanButtonText,
            textWeight: FontWeight.w200,
            textSize: 16,
          ),
        ),
//        Padding(
//          padding: EdgeInsets.only(left: 40.0, right: 40.0, bottom: 48.0),
//          child: CustomUnderlineText(
//            textWeight: FontWeight.w200,
//            textAlign: TextAlign.center,
//            textSize: 16,
//            textTitle: capturedData,
//            //Strings.partnerHomeHintId,
//          ),
//        ),
        CustomFlatButton(
          onPressed: () {
            AlertUtil.createTechHelpAlert(context, phone);
          },
          titleText: Strings.partnerHomeTechHelp,
          textWeight: FontWeight.w200,
          textSize: 12,
        ),
        CustomFlatButton(
          onPressed: () {
            _removePartnerFromCache();
          },
          titleText: Strings.partnerHomeLogout,
          textWeight: FontWeight.w200,
          textSize: 12,
        ),
      ],
    );
  }

  Future<void> _removePartnerFromCache() async {
    final prefs = await SharedPreferences.getInstance();
    cache.setRole(prefs, cache.Roles.notAuth);
    cache.removePartnerInfo(prefs);
    Navigator.of(context).pushNamedAndRemoveUntil(
        router.ROUTE_MAIN_AUTH, (Route<dynamic> route) => false);
    return;
  }

  Future<String> _getUserPhone() async {
    final prefs = await SharedPreferences.getInstance();
    String phone = cache.getPhone(prefs);
    return phone;
  }

  Future<String> _getPartnerName() async {
    final prefs = await SharedPreferences.getInstance();
    PartnerInfo partnerInfo = cache.getPartnerInfo(prefs);
    return partnerInfo.partnerName;
  }

  void _makeQRRequest(String customerId) async {
    ProgressDialog loader = AlertUtil.getLoader(context);
    int id;
    try {
      id = int.parse(customerId);
    } catch (err) {
      AlertUtil.createErrorAlertWithCallback(context, () {
        _pauseScan(true);
        setState(() {});
        Navigator.pop(context);
      });
      return;
    }
    loader.show();
    final prefs = await SharedPreferences.getInstance();
    var partner = cache.getPartnerInfo(prefs);
    http
        .post(getQRUrl(),
            body: formQRMap(id), headers: formHeader(partner.token))
        .then((response) {
      try {
        if (response.statusCode == 204 || response.statusCode == 200) {
          loader.hide().then((onValue) {
            AlertUtil.createQrSuccessAlert(context, () {
              _pauseScan(true);
              setState(() {});
              Navigator.pop(context);
            });
          });
        } else {
          loader.hide().then((onValue) {
            AlertUtil.createErrorAlertWithCallback(context, () {
              _pauseScan(true);
              setState(() {});
              Navigator.pop(context);
            });
          });
        }
      } catch (error) {
        loader.hide().then((onValue) {
        AlertUtil.createErrorAlertWithCallback(context, () {
          _pauseScan(true);
          setState(() {});
          Navigator.pop(context);
        });
        });
      }
    }).catchError((error) {
      loader.hide().then((onValue) {
        AlertUtil.createErrorAlertWithCallback(context, () {
          _pauseScan(true);
          setState(() {});
          Navigator.pop(context);
        });
    });
  });

}
}
