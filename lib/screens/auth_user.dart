import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:grad/components/outline_button.dart';
import 'package:grad/components/outline_text_field.dart';
import 'package:grad/components/text.dart';
import 'package:grad/resources/strings.dart';
import 'package:grad/utils/alert_util.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:grad/web/userApi.dart' as userAPI;
import 'package:grad/cache/SharedPrefsHelper.dart' as cache;
import 'package:grad/Routes.dart' as router;


class AuthUserScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return AuthUserScreenState();
  }
}

class AuthUserScreenState extends State<AuthUserScreen> {

  var textFieldEmpty = true;
  var buttonEnabled = false;
  var smsText;

  @override
  void initState() {
    super.initState();

    enableButton();
  }

  enableButton() {
    buttonEnabled = !textFieldEmpty;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(
              color: Colors.white
          ),
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: CustomText(
            textTitle: Strings.auth,
            textWeight: FontWeight.w200,
            textSize: 20,
          ),
          flexibleSpace: Image(
            image: AssetImage('assets/images/gradient_background.png'),
            fit: BoxFit.cover,
          ),
          backgroundColor: Colors.transparent,
          leading: IconButton(icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.popAndPushNamed(context, router.ROUTE_MAIN_AUTH),
          )
      ),
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/gradient_background.png'),
              fit: BoxFit.cover
          ),
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 32.0, right: 32.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                    flex: 0,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(
                            top: 32.0,
                            bottom: 32.0
                          ),
                          child: CustomText(
                            textTitle: Strings.authUserTitle,
                            textSize: 16,
                            textWeight: FontWeight.w200,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        CustomOutlineTextField(
                          maxLength: 4,
                          hintText: Strings.authUserInsertSmsCode,
                          textWeight: FontWeight.w200,
                          inputType: TextInputType.number,
                          onChanged: (text) {
                            setState(() {
                              textFieldEmpty = text == null || text.isEmpty || text.toString().length != 4;
                              smsText = text;
                              if (!textFieldEmpty) {
                                  _makeUserSmsConfirmRequest(smsText);
                              }
                              //enableButton();
                            });
                          },
                        ),
                        Padding(
                            padding: EdgeInsets.all(32.0),
                            child: SizedBox(
                              width: double.infinity,
                              // height: double.infinity,
                              child: Visibility(
                                visible: buttonEnabled,
                                  child: CustomOutlineButton(
                                    onPressed: () {
                                      _makeUserSmsConfirmRequest(smsText);
                                    },
                                    titleText: Strings.authUserDone,
                                    textWeight: FontWeight.w200,
                                    textSize: 20,
                                  )
                              ),
                            )
                        ),
                      ],
                    )
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _makeUserSmsConfirmRequest(String smsCode) async {
    ProgressDialog loader = AlertUtil.getLoader(context);
    loader.show();

    final prefs = await SharedPreferences.getInstance();
    http
        .post(userAPI.getUserSmsConfirmUrl(),
        body: userAPI.formUserSmsMap(smsCode, cache.getPhone(prefs)))
        .then((response) {

      loader.hide().then((onValue) {
      try {
        var decodedJson = jsonDecode(response.body);
          userAPI.UserSmsResponse userSmsResponse =
        userAPI.UserSmsResponse.fromJson(decodedJson);
        if (userSmsResponse.success == true) {
          _saveUserToCache(userSmsResponse.data);
          Navigator.pushReplacementNamed(context, router.ROUTE_USER_HOME);
        } else {
          AlertUtil.createErrorAlertWithCallback(context, () {
            Navigator.pop(context);
          });        }
      } catch (error) {
        AlertUtil.createErrorAlertWithCallback(context, () {
          Navigator.pop(context);
        });
      }
      });

    }).catchError((error) {
      loader.hide().then((onValue) {
        AlertUtil.createErrorAlertWithCallback(context, () {
          Navigator.pop(context);
        });
      });
    });
  }
}

Future<void> _saveUserToCache ( userAPI.UserInfo userInfo ) async {
  final prefs = await SharedPreferences.getInstance();
  cache.setRole(prefs, cache.Roles.authUser);
  cache.saveUserInfo(prefs, userInfo);
  return;
}