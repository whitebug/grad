import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:grad/components/pdf_viewer.dart';
import 'package:grad/components/underline_text.dart';
import 'package:grad/resources/urls.dart';
import 'package:grad/utils/alert_util.dart';
import 'package:grad/components/flat_button.dart';
import 'package:grad/components/outline_button.dart';
import 'package:grad/components/text.dart';
import 'package:grad/components/underline_button.dart';
import 'package:grad/components/underline_text_field.dart';
import 'package:grad/components/underline_text_phone_code.dart';
import 'package:grad/resources/strings.dart';
import 'package:grad/utils/url_opener_util.dart';
import 'package:http/http.dart' as http;
import 'package:grad/web/userApi.dart' as userAPI;
import 'package:path_provider/path_provider.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:grad/cache/SharedPrefsHelper.dart' as cache;
import 'package:grad/Routes.dart' as router;
import 'dart:developer' as developer;

class AuthScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AuthScreenState();
  }
}

class AuthScreenState extends State<AuthScreen> {
  var isCheck = true;
  var textFieldEmpty = true;
  var isUpdateMask = false;
  var buttonEnabled = false;
  var phone;

  var controller = new MaskedTextController(mask: '000 000 0000');

//  var controller = new TextEditingController();

  @override
  void initState() {
    super.initState();
    enableButton();
    _saveFirstLaunch();
  }

  enableButton() {
    buttonEnabled = isCheck && !textFieldEmpty;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/gradient_background.png'),
              fit: BoxFit.cover),
        ),
        child: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 32.0, right: 32.0),
              child: Column(
                children: <Widget>[
                  Image(
                    image: Image(image: AssetImage('assets/images/logo.png'))
                        .image,
                  ),
                  Expanded(
                      flex: 0,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 16.0),
                            child: CustomText(
                              textTitle: Strings.authTitle,
                              textSize: 16,
                              textWeight: FontWeight.w200,
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  SizedBox(height: 14),
                                  Container(
                                    width: 40,
                                    child: CustomUnderlineTextPhoneCode(
                                      textTitle: Strings.authPhoneCode,
                                      textSize: 16,
                                      textWeight: FontWeight.w200,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Container(
                                    height: 1,
                                    width: 40,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                              SizedBox(width: 15),
                              Expanded(
                                  child: CustomUnderlineTextField(
                                controller: controller,
                                hintText: Strings.authInsertPhone,
                                textWeight: FontWeight.w200,
                                textSize: 16,
                                inputType: TextInputType.number,
                                onChanged: (newtext) {
                                  setState(() {
                                    textFieldEmpty =
                                        newtext == null || newtext.isEmpty;
                                    phone = newtext;
                                    phone = phone
                                        .replaceAll(RegExp(r' '), '');
                                    if (phone.length > 10) {
                                      phone = phone.toString().substring(0, 10);
                                    }
                                    phone = Strings.authPhoneCode + phone;
                                    enableButton();
                                  });
                                },
                              )),
                            ],
                          ),
                          Center(
                            child: Row(
                              children: <Widget>[
                                Checkbox(
                                  value: isCheck,
                                  onChanged: (newValue) {
                                    setState(() {
                                      isCheck = newValue;
                                      enableButton();
                                    });
                                  }, //  <-- leading Checkbox
                                ),
                                Expanded(
                                  child: CustomFlatButton(
                                    titleText: Strings.authAgreement,
                                    textSize: 12,
                                    textWeight: FontWeight.w200,
                                    onPressed: () {
                                      Navigator.pushNamed(
                                          context, router.ROUTE_USER_AGREEMENT);
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.only(left: 32.0, right: 32.0),
                              child: SizedBox(
                                  width: double.infinity,
                                  // height: double.infinity,
                                  child: CustomOutlineButton(
                                    onPressed: buttonEnabled
                                        ? () {
                                            _makeUserPhoneRequest(phone);
                                          }
                                        : null,
                                    titleText: Strings.authNext,
                                    textWeight: FontWeight.w200,
                                    textSize: 20,
                                  ))),
                          CustomUnderlineButton(
                            titleText: Strings.authPartner,
                            onPressed: () {
                              Navigator.pushNamed(
                                  context, router.ROUTE_PARTNER_CODE);
                            },
                            textWeight: FontWeight.w200,
                            textSize: 16,
                          ),
                          CustomFlatButton(
                            onPressed: () {
                              //UrlOpenerUtil.launchURL(Urls.urlSite);
                              AlertUtil.createTechHelpAlert(context, '');
                            },
                            titleText: Strings.authFeedback,
                            textWeight: FontWeight.w200,
                            textSize: 12,
                          ),
                          CustomFlatButton(
                            onPressed: () {
                              //UrlOpenerUtil.launchURL(Urls.urlHome);
                              preparePdf(pdf: 'assets/pdf/offer.pdf').then((path) {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PdfViewer(path)),
                                );
                              });
                            },
                            titleText: Strings.authPublicOffer,
                            textWeight: FontWeight.w200,
                            textSize: 12,
                          ),
                          CustomFlatButton(
                            onPressed: () {
                              // UrlOpenerUtil.launchURL(Urls.urlRules);
                              preparePdf(pdf: 'assets/pdf/rules.pdf').then((path) {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PdfViewer(path)),
                                );
                              });
                            },
                            titleText: Strings.authRules,
                            textWeight: FontWeight.w200,
                            textSize: 12,
                          ),
                        ],
                      )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<File> copyAsset({@required String pdf}) async {
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    File tempFile = File('$tempPath/copy.pdf');
    ByteData bd = await rootBundle.load(pdf);
    await tempFile.writeAsBytes(bd.buffer.asUint8List(), flush: true);
    return tempFile;
  }

  Future<String> preparePdf({@required String pdf}) async {
    final ByteData bytes =
    await DefaultAssetBundle.of(context).load(pdf);
    final Uint8List list = bytes.buffer.asUint8List();

    final tempDir = await getTemporaryDirectory();
    final tempDocumentPath = '${tempDir.path}/$pdf';

    final file = await File(tempDocumentPath).create(recursive: true);
    file.writeAsBytesSync(list);
    return tempDocumentPath;
  }

  void _makeUserPhoneRequest(String userPhone) async {
    ProgressDialog loader = AlertUtil.getLoader(context);
    loader.show();
    http
        .post(userAPI.getUserRegistrationUrl(),
            body: userAPI.formUserPhoneMap(userPhone))
        .then((response) async {
      loader.hide().then((onValue) async {
        try {
          var decodedJson = jsonDecode(response.body);
          userAPI.UserRegisterResponse userRegisterResponse =
              userAPI.UserRegisterResponse.fromJson(decodedJson);
          if (userRegisterResponse.success == true) {
            final prefs = await SharedPreferences.getInstance();
            cache.savePhone(prefs, userPhone);
            Navigator.pushNamed(context, router.ROUTE_USER_SMS);
          } else {
            AlertUtil.createErrorAlertWithCallback(context, () {
              Navigator.pop(context);
            });
          }
        } catch (error) {
          AlertUtil.createErrorAlertWithCallback(context, () {
            Navigator.pop(context);
          });
        }
      });
    }).catchError((error) {
      loader.hide().then((onValue) {
        AlertUtil.createErrorAlertWithCallback(context, () {
          Navigator.pop(context);
        });
      });
    });
  }

  Future<void> _saveFirstLaunch() async {
    final prefs = await SharedPreferences.getInstance();
    cache.setRole(prefs, cache.Roles.notAuth);
    return;
  }
}

