import 'package:flutter/material.dart';
import 'package:grad/components/text.dart';
import 'package:grad/components/underline_button.dart';
import 'package:grad/resources/urls.dart';
import 'package:grad/utils/url_opener_util.dart';
import '../resources/strings.dart';

class DevelopersScreen extends StatefulWidget {
  @override
  DevelopersScreenState createState() => DevelopersScreenState();
}

class DevelopersScreenState extends State<DevelopersScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: CustomText(
            textTitle: Strings.developersTitle,
            textWeight: FontWeight.w200,
            textSize: 20,
          ),
          iconTheme: IconThemeData(color: Colors.white),
          flexibleSpace: Image(
            image: AssetImage('assets/images/gradient_background.png'),
            fit: BoxFit.cover,
          ),
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.pop(context, false),
          )),
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/gradient_background.png'),
              fit: BoxFit.cover),
        ),
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            Container(
              child: Align(
                alignment: Alignment.topCenter,
                child: _setupWidgets(), //_buildToolBar(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _setupWidgets() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 16, left: 32, right: 32, top: 100),
          child: Image(
            image:
                Image(image: AssetImage('assets/images/developers_logo_1.png'))
                    .image,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 8, top: 40),
          child: CustomText(
            textTitle: Strings.developersDeveloper,
            textWeight: FontWeight.w200,
            textSize: 14,
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 40),
          child: CustomUnderlineButton(
            onPressed: () {
              UrlOpenerUtil.launchURL(Urls.urlDevelopersFirst);
            },
            titleText: Strings.developersOpenUrlFirst,
            textWeight: FontWeight.w200,
            textSize: 14,
          ),
        ),
      ],
    );
  }
}
