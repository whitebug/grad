import 'package:flutter/material.dart';
import 'package:grad/components/text.dart';
import 'package:grad/resources/strings.dart';

class UserAgreementPage extends StatefulWidget {
  UserAgreementPage({Key key}) : super(key: key);

  @override
  _UserAgreementPageState createState() => _UserAgreementPageState();
}

class _UserAgreementPageState extends State<UserAgreementPage> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
            color: Colors.white
        ),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: CustomText(
          textTitle: Strings.agreementTitle,
          textWeight: FontWeight.w200,
          textSize: 20,
        ),
        flexibleSpace: Image(
          image: AssetImage('assets/images/gradient_background.png'),
          fit: BoxFit.cover,
        ),
        backgroundColor: Colors.transparent,
        leading: IconButton(icon:Icon(Icons.arrow_back_ios),
          onPressed:() => Navigator.pop(context, false),
        )
      ),
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/gradient_background.png'),
              fit: BoxFit.cover
          ) ,
        ),
        child: SingleChildScrollView(
             child: Center(
            child: Padding(
              padding: EdgeInsets.all(32.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  CustomText(
                    textTitle: Strings.agreementText,
                    textSize: 20,
                    textWeight: FontWeight.w300,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),// This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}