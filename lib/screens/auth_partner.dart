import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:grad/components/outline_button.dart';
import 'package:grad/components/text.dart';
import 'package:grad/components/underline_button.dart';
import 'package:grad/components/underline_text_field.dart';
import 'package:grad/resources/strings.dart';
import 'package:grad/utils/alert_util.dart';
import 'package:http/http.dart' as http;
import 'package:grad/web/partnerAPI.dart' as partnerAPI;
import 'package:grad/cache/SharedPrefsHelper.dart' as cache;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:grad/Routes.dart' as router;

class AuthPartnerScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AuthPartnerScreenState();
  }
}

class AuthPartnerScreenState extends State<AuthPartnerScreen> {
  var textFieldEmpty = true;
  var buttonEnabled = false;
  var enteredCode;

  @override
  void initState() {
    super.initState();

    enableButton();
  }

  enableButton() {
    buttonEnabled = !textFieldEmpty;
  }

  @override
  Widget build(BuildContext context) {
     print("build auth");
    return Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: CustomText(
            textTitle: '',
            textWeight: FontWeight.w200,
            textSize: 20,
          ),
          flexibleSpace: Image(
            image: AssetImage('assets/images/gradient_background.png'),
            fit: BoxFit.cover,
          ),
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () =>
                Navigator.popAndPushNamed(context, router.ROUTE_MAIN_AUTH),
          )),
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/gradient_background.png'),
              fit: BoxFit.cover),
        ),
        //child: Center(
        child: Padding(
          padding: EdgeInsets.only(left: 32.0, right: 32.0),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Expanded(
                    flex: 0,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 32.0, bottom: 32.0),
                          child: CustomText(
                            textTitle: Strings.authPartnerTitle,
                            textSize: 20,
                            textWeight: FontWeight.w200,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 48.0),
                          child: CustomText(
                            textTitle: Strings.authPartnerSubtitle,
                            textSize: 16,
                            textWeight: FontWeight.w200,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        CustomUnderlineTextField(
                          hintText: Strings.authPartnerCodeHint,
                          textWeight: FontWeight.w200,
                          textSize: 16,
                          inputType: TextInputType.number,
                          onChanged: (text) {
                            setState(() {
                              textFieldEmpty = text == null || text.isEmpty;
                              enteredCode = text;
                              enableButton();
                            });
                          },
                        ),
                        Padding(
                            padding: EdgeInsets.only(
                                left: 32.0,
                                right: 32.0,
                                top: 32.0,
                                bottom: 32.0),
                            child: SizedBox(
                                width: double.infinity,
                                // height: double.infinity,
                                child: CustomOutlineButton(
                                  onPressed: buttonEnabled
                                      ? () {
                                          _makePartnerRequest(enteredCode);
                                        }
                                      : null,
                                  titleText: Strings.authPartnerLogin,
                                  textWeight: FontWeight.w200,
                                  textSize: 20,
                                ))),
                        CustomUnderlineButton(
                          titleText: Strings.authPartnerBeAPartner,
                          onPressed: () {
                            AlertUtil.createPartnerRequestAlert(context);
                          },
                          textWeight: FontWeight.w200,
                          textSize: 16,
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ),
      ),
      //),
    );
  }

  void _makePartnerRequest(String partnerCode) async {
    ProgressDialog loader = AlertUtil.getLoader(context);
    loader.show();
    http
        .post(partnerAPI.getPartnerAuthUrl(),
            body: partnerAPI.formPartnerCodeMap(partnerCode))
        .then((response) {
      loader.hide().then((onValue) {
        try {
          var decodedJson = jsonDecode(response.body);
          partnerAPI.PartnerAuthResponse partnerAuthResponse =
              partnerAPI.PartnerAuthResponse.fromJson(decodedJson);
          if (partnerAuthResponse.success == true) {
            _savePartnerToCache(partnerAuthResponse.data);
            Navigator.pushReplacementNamed(context, router.ROUTE_PARTNER_HOME);
          } else {
            AlertUtil.createErrorAlertWithCallback(context, () {
              Navigator.pop(context);
            });
          }
        } catch (error) {
          AlertUtil.createErrorAlertWithCallback(context, () {
            Navigator.pop(context);
          });
        }
      });
    }).catchError((error) {
      loader.hide().then((onValue) {
        AlertUtil.createErrorAlertWithCallback(context, () {
          Navigator.pop(context);
        });
      });
    });
  }

  Future<void> _savePartnerToCache(partnerAPI.PartnerInfo partnerInfo) async {
    final prefs = await SharedPreferences.getInstance();
    cache.setRole(prefs, cache.Roles.authPartner);
    cache.savePartnerInfo(prefs, partnerInfo);
    return;
  }
}
