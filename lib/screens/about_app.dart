import 'package:flutter/material.dart';
import 'package:grad/components/outline_button.dart';
import 'package:grad/components/page_view_indicator.dart';
import 'package:grad/components/text.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:grad/cache/SharedPrefsHelper.dart' as cache;
import 'package:grad/resources/strings.dart';
import 'package:grad/Routes.dart' as router;
import 'package:video_player/video_player.dart';

class AboutAppScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AboutAppScreenState();
  }
}

class AboutAppScreenState extends State<AboutAppScreen> {
  final PageController controller = PageController(initialPage: 0);

  VideoPlayerController _controller;

  bool buttonPlayVisible = true;
  bool buttonPauseVisible = false;

  void _pageChanged(int index) {
    print('___________  pageIndex: $index');
    if (_controller != null){
      _enablePlayButton(true);
      _controller.pause();
    }
    setState(() {});
  }

  void _enablePlayButton(bool enable) {
    this.buttonPlayVisible = enable;
    this.buttonPauseVisible = !enable;
  }

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.asset('assets/videos/about_app.mp4')
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });
    _controller.setLooping(true);
    _enablePlayButton(true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(
              color: Colors.white
          ),
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: CustomText(
            textTitle: Strings.menuAboutApp,
            textWeight: FontWeight.w200,
            textSize: 20,
          ),
          flexibleSpace: Image(
            image: AssetImage('assets/images/gradient_background.png'),
            fit: BoxFit.cover,
          ),
          backgroundColor: Colors.transparent,
          leading: IconButton(icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.pop(context, false),
          )
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/gradient_background.png'),
              fit: BoxFit.cover),
        ),
        child: Center(
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Container(
                //height: 250,
                child: PageView(
                  onPageChanged: _pageChanged,
                  controller: controller,
                  children: <Widget>[
                    getPageSimpleWidget('assets/images/about_app_page_0.png',
                        Strings.aboutAppPageOneTitle),
                    getPageSimpleWidget('assets/images/about_app_page_3.png',
                        Strings.aboutAppPageThreeTitle),
                    getPageSimpleWidget('assets/images/about_app_page_2.png',
                        Strings.aboutAppPageTwoTitle),
                    getPageVideoWidget(),
                  ],
                ),
              ),
              Indicator(
                controller: controller,
                itemCount: 4,
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
    controller.dispose();
  }

  Widget getPageSimpleWidget(String image, String text) {
    return new Padding(
      padding: EdgeInsets.only(top: 64, bottom: 64, left: 32, right: 32),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Padding(
                padding: EdgeInsets.only(left: 32, right: 32, bottom: 32),
                child: Image(
                  height: 250,
                  image: Image(image: AssetImage(image)).image,
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: EdgeInsets.only(top: 32),
                child: CustomText(
                  textTitle: text,
                  textSize: 20,
                  textWeight: FontWeight.w200,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(),
                ),
                Expanded(
                  flex: 4,
                  child: CustomOutlineButton(
                    onPressed: () {
                      controller.nextPage(
                        duration: kTabScrollDuration,
                        curve: Curves.ease,
                      );
                    },
                    titleText: Strings.aboutAppNextButton,
                    textWeight: FontWeight.w200,
                    textSize: 20,
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget getPageVideoWidget() {
    return new Padding(
        padding: EdgeInsets.all(42.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Center(
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 32),
                    child: Wrap(
                      children: <Widget>[
                        Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              Center(
                                child: _controller.value.initialized
                                    ? AspectRatio(
                                  aspectRatio: _controller.value.aspectRatio,
                                  child: VideoPlayer(_controller),
                                )
                                    : Container(),
                              ),
                              Visibility(
                                visible: buttonPlayVisible,
                                child: FloatingActionButton(
                                  onPressed: () {
                                    setState(() {
                                      _enablePlayButton(
                                          _controller.value.isPlaying);
                                      _controller.value.isPlaying
                                          ? _controller.pause()
                                          : _controller.play();
                                    });
                                  },
                                  child: Icon(
                                    Icons.play_arrow,
                                  ),
                                ),
                              ),
                              Positioned(
                                top: 0,
                                right: 0,
                                child: Visibility(
                                  visible: buttonPauseVisible,
                                  child: FloatingActionButton(
                                    onPressed: () {
                                      setState(() {
                                        _enablePlayButton(
                                            _controller.value.isPlaying);
                                        _controller.value.isPlaying
                                            ? _controller.pause()
                                            : _controller.play();
                                      });
                                    },
                                    backgroundColor: Colors.transparent,
                                    child: Icon(
                                      Icons.close,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ]
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Wrap(

                  children: <Widget>[
                    Container(
                      width: double.maxFinite,
                      child: Padding(
                          padding: EdgeInsets.only(top: 32),
                          child: CustomOutlineButton(
                            onPressed: () {
                              _controller.pause();
                              Navigator.pop(context);
                            },
                            titleText: Strings.aboutAppDoneButton,
                            textWeight: FontWeight.w200,
                            textSize: 20,
                          )
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
    );
  }

  Widget makePlayButton() {
    return new Center(
      child: Image(
        width: 64,
        height: 64,
        image: Image(image: AssetImage('assets/images/play_button_background.png')).image,
      ),
    );
  }
}