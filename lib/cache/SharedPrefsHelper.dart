import 'package:grad/web/partnerAPI.dart';
import 'package:grad/web/userApi.dart';
import 'package:shared_preferences/shared_preferences.dart';

const USER_PHONE_KEY = 'user_phone';
const ROLE_KEY = 'role';
const USER_ID = "user_id";
const USER_CREATE_DATE = "user_create_date";
const USER_TOKEN = "user_token";
const PARTNER_ID = "partner_id";
const PARTNER_CREATE_DATE = "partner_create_date";
const PARTNER_TOKEN = "partner_token";
const PARTNER_NAME = 'partner_name';

/**
 * phone
 */
void savePhone ( SharedPreferences prefs, userPhone ) => prefs.setString ( USER_PHONE_KEY, userPhone );

String getPhone ( SharedPreferences prefs ) => prefs.getString ( USER_PHONE_KEY );

void removePhone ( SharedPreferences prefs ) => prefs.remove ( USER_PHONE_KEY );

/**
 *
 * {@link [Roles}}
 * roles {0, 1, 2}
 *
 * notAuth - new user or
 * authUser - authorized user
 * authPartner - authorized partner
 */

void setRole ( SharedPreferences prefs, Roles role ) => prefs.setInt(ROLE_KEY, _rolesValue(role));

Future getRole ( SharedPreferences prefs ) async {
  int roleInt = prefs.getInt(ROLE_KEY);
  if ( roleInt == null ) {
    roleInt = 0;
    setRole(prefs, Roles.firstLaunch);
  }
  var role = await _rolesByValue(roleInt);
  return role;
}

enum Roles { firstLaunch, notAuth, authUser, authPartner }

int _rolesValue( Roles role ) {
  switch (role) {
    case Roles.firstLaunch:
      return 0;
    case Roles.notAuth:
      return 1;
    case Roles.authUser:
      return 2;
    case Roles.authPartner:
      return 3;
  }
}

Future<Roles> _rolesByValue( int roleInt ) async {
  switch (roleInt) {
    case 0:
      return Roles.firstLaunch;
    case 1:
      return Roles.notAuth;
    case 2:
      return Roles.authUser;
    case 3:
      return Roles.authPartner;
  }
}

/**
 * UserInfo
 *
*/

void saveUserInfo ( SharedPreferences prefs, UserInfo userInfo ) {
  prefs.setInt ( USER_ID, userInfo.customerId );
  prefs.setString( USER_CREATE_DATE, userInfo.createDate );
  prefs.setString( USER_TOKEN, userInfo.token );
}

UserInfo getUserInfo ( SharedPreferences prefs ) {
  UserInfo userInfo = UserInfo();
  userInfo.customerId = prefs.getInt ( USER_ID );
  userInfo.token = prefs.getString ( USER_TOKEN );
  userInfo.token = prefs.getString ( USER_TOKEN );
  return userInfo;
}

void removeUserInfo ( SharedPreferences prefs ) {
  prefs.remove ( USER_ID );
  prefs.remove ( USER_CREATE_DATE );
  prefs.remove ( USER_TOKEN );
}

/**
 * PartnerInfo
 */

void savePartnerInfo ( SharedPreferences prefs, PartnerInfo partnerInfo ) {
  prefs.setInt ( PARTNER_ID, partnerInfo.partnerId );
  prefs.setString( PARTNER_CREATE_DATE, partnerInfo.createDate );
  prefs.setString( PARTNER_TOKEN, partnerInfo.token );
  prefs.setString(PARTNER_NAME, partnerInfo.partnerName);
}

PartnerInfo getPartnerInfo ( SharedPreferences prefs ) {
  PartnerInfo partnerInfo = PartnerInfo();
  partnerInfo.partnerId = prefs.getInt ( PARTNER_ID );
  partnerInfo.token = prefs.getString ( PARTNER_CREATE_DATE );
  partnerInfo.token = prefs.getString ( PARTNER_TOKEN );
  partnerInfo.partnerName = prefs.getString(PARTNER_NAME);
  return partnerInfo;
}

void removePartnerInfo( SharedPreferences prefs ) {
  prefs.remove ( PARTNER_ID );
  prefs.remove ( PARTNER_CREATE_DATE );
  prefs.remove ( PARTNER_TOKEN );
  prefs.remove(PARTNER_NAME);
}