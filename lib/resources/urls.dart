class Urls {
  static final String urlHome = 'https://remontgrad-sochi.ru/#partner';
  static final String urlOffer = 'https://remontgrad-sochi.ru/oferta/';
  static final String urlRules = 'https://remontgrad-sochi.ru/pravila-uchastiya/';
  static final String urlPartners = 'https://remontgrad-sochi.ru/vse-partnery/';
  static final String urlDevelopersFirst = 'https://tebenev.pro';
  static final String urlDevelopersSecond = 'http://oneapp-studio.ru';
  static final String urlInstagram = 'https://www.instagram.com/rg123ru/?igshid=1cd1s6bzmeyzi';
}
