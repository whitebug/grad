class Strings {
  //common
  static final String appName = "РемонтГрад";
  static final String auth = 'Авторизация';
  static final String register = 'Регистрация';

  //splash
  static final String splashText =
      'РЕМОНТГРАД - это способ сделать ремонт комфортней и выгодней';

  //user agreement
  static final String agreementTitle = 'Согласие на обработку данных';
  static final String agreementText =
      'Администрация сайта http://rg123.ru (далее Сайт) с уважением относится к правам посетителей Сайта. Мы безоговорочно признаем важность конфиденциальности личной информации посетителей нашего Сайта. Данная страница содержит сведения о том, какую информацию мы получаем и собираем, когда Вы пользуетесь Сайтом. Настоящая Политика конфиденциальности распространяется только на Сайт и на информацию, собираемую этим сайтом и через его посредство. Она не распространяется ни на какие другие сайты и не применима к веб-сайтам третьих лиц, с которых могут делаться ссылки на Сайт.\n\nСбор информации\n\nКогда Вы посещаете Сайт, мы определяем имя домена Вашего провайдера, страну и выбранные переходы с одной страницы на другую (так называемую \"активность потока переходов\"). Сведения, которые мы получаем на Сайте, могут быть использованы для того, чтобы облегчить Вам пользование Сайтом. Сайт собирает только личную информацию, которую Вы предоставляете добровольно при посещении Сайта. Понятие \"личная информация\" включает информацию, которая определяет Вас как конкретное лицо, например, Ваше имя или адрес электронной почты.\n\nТехнология \"cookies\"\n\nСайт применяет технологию \"cookies\" (\"куки\") для создания статистической отчетности. \"Куки\" представляет собой небольшой объем данных, отсылаемый веб-сайтом, который браузер Вашего компьютера сохраняет на жестком диске Вашего же компьютера. В \"cookies\" содержится информация, которая может быть необходимой для Сайта, - для сохранения Ваших установок вариантов просмотра и сбора статистической информации по Сайту, т.е. какие страницы Вы посетили, что было загружено, имя домена интернет-провайдера и страна посетителя, а также адреса сторонних веб-сайтов, с которых совершен переход на Сайт и далее. Однако вся эта информация никак не связана с Вами как с личностью. \"Cookies\" не записывают Ваш адрес электронной почты и какие-либо личные сведения относительно Вас. Также данную технологию на Сайте использует установленный счетчик компании Spylog/LiveInternet/и т.п. Кроме того, мы используем стандартные журналы учета веб-сервера для подсчета количества посетителей и оценки технических возможностей нашего Сайта. Мы используем эту информацию для того, чтобы определить, сколько человек посещает Сайт и организовать страницы наиболее удобным для пользователей способом, обеспечить соответствие Сайта используемым браузерам, и сделать содержание наших страниц максимально полезным для наших посетителей. Мы записываем сведения по перемещениям на Сайте, но не об отдельных посетителях Сайта, так что никакая конкретная информация относительно Вас лично не будет сохраняться или использоваться Администрацией Сайта без Вашего согласия. Чтобы просматривать материал без \"cookies\", Вы можете настроить свой браузер таким образом, чтобы она не принимала \"cookies\" либо уведомляла Вас об их посылке (различны, поэтому советуем Вам справиться в разделе \"Помощь\" и выяснить, как изменить установки машины по \"cookies\").\nСовместное использование информации.\nАдминистрация Сайта ни при каких обстоятельствах не продает и не отдает в пользование Вашу личную информацию, каким бы то ни было третьим сторонам. Мы также не раскрываем предоставленную Вами личную информацию за исключением случаев, предусмотренных законодательством (укажите страну, где расположен сервер). Администрация сайта имеет партнерские отношения с компанией Google, которая размещает на возмездной основе на страницах сайта рекламные материалы и объявления (включая, но не ограничиваясь, текстовые гиперссылки). В рамках данного сотрудничества Администрация сайта доводит до сведения всех заинтересованных сторон следующую информацию:компания Google как сторонний поставщик использует файлы cookie для показа объявлений на Сайте; файлы cookie рекламных продуктов DoubleClick DART используются Google в объявлениях, показываемых на Сайте, как участнике программы AdSense для контента. использование компанией Google файлов cookie DART позволяет ей собирать и использовать информацию о посетителях Сайта (за исключением имени, адреса, адреса электронной почты или номера телефона), их посещениях Сайта и других веб-сайтов с целью предоставления наиб��лее релевантных объявлений о товарах и услугах. компания Google в процессе сбора данной информации руководствуется собственной политикой конфиденциальности; пользователи Сайта могут отказаться от использования файлов cookie DART, посетив страницу с политикой конфиденциальности для объявлений и сети партнерских сайтов Google.\n\nОтказ от ответственности\n\nПомните, передача информации личного характера при посещении сторонних сайтов, включая сайты компаний-партнеров, даже если веб-сайт содержит ссылку на Сайт или на Сайте есть ссылка на эти веб-сайты, не подпадает под действия данного документа. Администрация Сайта не несет ответственности за действия других веб-сайтов. Процесс сбора и передачи информации личного характера при посещении этих сайтов регламентируется документом «Защита информации личного характера» или аналогичным, расположенном на сайтах этих компаний.';
  //menu drawer
  static final String menuMain = 'Главная';
  static final String menuPartners = 'Партнеры';
  static final String menuTechHelp = 'Техническая поддержка';
  static final String menuRules = 'Правила пользования';
  static final String menuOffer = 'Оферта';
  static final String menuQuit = 'Выйти из аккаунта';
  static final String menuAboutApp = 'О приложении';
  static final String menuInstagram = 'Наш инстаграм';

  //bottomMenu
  static final String bottomMenuQrCode = 'Qr-код';
  static final String bottomMenuCategories = 'Категории';
  static final String bottomMenuPartners = menuPartners;

  //auth
  static final String authTitle =
      'Для авторизации или регистрации введите номер телефона';
  static final String authPhoneCode = '+7';
  static final String authInsertPhone = 'Введите номер телефона';
  static final String authAgreement =
      'Согласие на обработку персональных данных';
  static final String authNext = 'Продолжить';
  static final String authPartner = 'Войти как партнер';
  static final String authFeedback = 'Обратная связь';
  static final String authPublicOffer = 'Публичная оферта';
  static final String authRules = 'Правила участия в программе';

  //auth user
  static final String authUserTitle =
      'На ваш номер телефона было отправленно СМС с кодом подтверждения\nВведите этот код';
  static final String authUserInsertSmsCode = 'Введите Код из СМС';
  static final String authUserDone = 'Готово';

  //auth user
  static final String authPartnerTitle = 'Кто такой партнёр?';
  static final String authPartnerSubtitle =
      'Это компания, предоставляющая товары или услуги в сфере строительства, отделки и декора';
  static final String authPartnerCodeHint = 'Специальный код партнера';
  static final String authPartnerLogin = 'Войти';
  static final String authPartnerBeAPartner = 'Хочу стать партнером';

  //alert error
  static final String alertErrorTitle = 'Произошла ошибка';
  static final String alertErrorButton = 'Готово';

  //alert qr success
  static final String alertQrSuccessTitle =
      'QR код успешно отсканирован\nПокупка зафиксированна';
  static final String alertQrSuccessButton = 'Готово';

  //alert tech help
  static final String alertTechHelpTitle = 'Техническая поддержка';
  static final String alertTechHelpSubtitle =
      'Напишите ваше обращение в техническую поддержку';
  static final String alertTechHelpHint = 'Обращение';
  static final String alertTechHelpHintName = 'Имя';
  static final String alertTechHelpHintPhone = '+7';
  static final String alertTechHelpButtonClose = 'Назад';
  static final String alertTechHelpButtonOk = 'Готово';
  static final String alertTechHelpSucces = 'Отправлено'; //'Заявка отправлена';

  //alert partner request
  static final String alertPartnerRequestTitle = 'Оставить заявку';
  static final String alertPartnerRequestHintName = 'Имя';
  static final String alertPartnerRequestHintPhone = 'Телефон';
  static final String alertPartnerRequestButtonClose = 'Закрыть';
  static final String alertPartnerRequestButtonSend = 'Отправить';
  static final String alertSuccesFullPartnerRequset =
      'Отправлено'; //'Заявка успешно принята';

  //user main screen
  static final String userHomeShowYourQR =
      'Для получения скидки -\n покажите продавцу';
  static final String userHomeShowAllPartners = 'Посмотреть всех партнеров';

  //partner home
  static final String partnerHomeTitleStartQr =
      'Начать сканирование'; //'Считать QR код';
  static final String partnerHomeTitleStopQr = 'Остановить сканирование';
  static final String partnerHomeHintId = 'ID покупателя';
  static final String partnerHomeTechHelp = 'Техническая поддержка';
  static final String partnerHomeLogout = 'Выйти из аккаунта';

  //custom alert
  static final String customAlertOk = 'Ок';

  //custom loader
  static final String loaderTilte = "Загрузка...";

  //developers
  static final String developersTitle = 'Разработчики';
  static final String developersOpenUrlFirst =
      'Перейти на страницу\n«ЦЕХ ПРОЖАРКИ БИЗНЕСА»';
  static final String developersOpenUrlSecond = 'Перейти на страницу\n«OneApp»';
  static final String developersDeveloper =
      'Руководитель проекта Данил Мельник';

  //about app
  static final String aboutAppPageOneTitle =
      'Данное приложение\nпозволяет Вам\nпользоваться скидками\nв магазинах-партнерах\nРемонтГрад';
  static final String aboutAppPageTwoTitle =
      'Чтобы посмотреть\nпартнеров и их адреса,\nа также размер\nВашей скидки -\nперейдите по ссылке\n\"Посмотреть всех\nпартнеров\"';
  static final String aboutAppPageThreeTitle =
      'Для того чтобы\nвоспользоваться скидкой -\nпокажите продавцу\nна кассе QR-код\nОн его считает -\nи вы получите скидку';
  static final String aboutAppDoneButton = 'Готово';
  static final String aboutAppNextButton = 'Далее';
}
