import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:grad/components/underline_text.dart';

class CustomUnderlineButton extends StatelessWidget {
  CustomUnderlineButton({
    @required this.onPressed,
    @required this.titleText,
    @required this.textSize,
    @required this.textWeight,
    this.titleAlign
  });
  final GestureTapCallback onPressed;
  final String titleText;
  final double textSize;
  final FontWeight textWeight;
  final TextAlign titleAlign;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: CustomUnderlineText(
        textTitle: titleText,
        textWeight: textWeight,
        textSize: textSize,
        textAlign: titleAlign != null ? titleAlign : TextAlign.center,
      ),
      onPressed: onPressed,
    );
  }
}