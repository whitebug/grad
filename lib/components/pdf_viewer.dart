import 'package:flutter/material.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';

/// Pdf viewer page
class PdfViewer extends StatelessWidget {
  final String pdfPath;

  PdfViewer(this.pdfPath);

  @override
  Widget build(BuildContext context) {
    return PDFViewerScaffold(
        appBar: AppBar(
          title: Text(''),
        ),
        path: pdfPath);
  }
}