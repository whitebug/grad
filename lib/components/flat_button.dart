import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:grad/components/text.dart';

class CustomFlatButton extends StatelessWidget {
  CustomFlatButton({
    @required this.onPressed,
    @required this.titleText,
    @required this.textSize,
    @required this.textWeight
  });
  final GestureTapCallback onPressed;
  final String titleText;
  final double textSize;
  final FontWeight textWeight;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      child: CustomText(
        textTitle: titleText,
        textSize: textSize,
        textWeight: textWeight,
      ),
      onPressed: onPressed,
    );
  }
}