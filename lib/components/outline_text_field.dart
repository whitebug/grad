import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomOutlineTextField extends StatelessWidget {
  CustomOutlineTextField({
    @required this.onChanged,
    @required this.hintText,
    @required this.textWeight,
    this.inputType,
    this.maxLength = 100,
    this.controller
  });
  final Function onChanged;
  final String hintText;
  final FontWeight textWeight;
  final TextInputType inputType;
  final int maxLength;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {

    var formatterList = List<LengthLimitingTextInputFormatter>();
    formatterList.add(LengthLimitingTextInputFormatter(maxLength));

    return TextField(
      controller: controller,
      onChanged: onChanged,
      textAlign: TextAlign.center,
      keyboardType: inputType,
      inputFormatters: formatterList,
      style: TextStyle(
        decoration: TextDecoration.none,
        fontFamily: "Gotham",
        fontWeight: textWeight,
        color: Colors.white,
      ),
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(0.0)
        ),
        border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(0.0)
        ),
        hintText: hintText,
        hintStyle: TextStyle(
          color: Colors.grey,
        ),
        labelStyle: TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }
}