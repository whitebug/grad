import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomUnderlineTextField extends StatelessWidget {
  CustomUnderlineTextField({
    @required this.inputFormatters,
    @required this.onChanged,
    @required this.hintText,
    @required this.textWeight,
    @required this.textSize,
    this.inputType,
    this.controller
  });
  final Function(String text) onChanged;
  final String hintText;
  final FontWeight textWeight;
  final double textSize;
  final TextInputType inputType;
  final TextEditingController controller;
  final List<TextInputFormatter> inputFormatters;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      onChanged: onChanged,
      keyboardType: inputType,
      textAlign: TextAlign.center,
      inputFormatters: inputFormatters,
      autofocus: false,
      cursorColor: Colors.grey,
      style: TextStyle(
        decoration: TextDecoration.none,
        fontFamily: "Gotham",
        fontWeight: textWeight,
        color: Colors.white,
      ),
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white)
        ),
        border: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white)
        ),
        hintText: hintText,
        hintStyle: TextStyle(
          color: Colors.grey,
        ),
        labelStyle: TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }
}