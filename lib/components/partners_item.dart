import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grad/utils/cards_util.dart';
import 'package:grad/web/categoriesAPI.dart';
import 'package:grad/web/webApi.dart';

class SalePainter extends CustomPainter {
  final Color strokeColor;
  final PaintingStyle paintingStyle;
  final double strokeWidth;

  SalePainter({
    this.strokeColor = Colors.black,
    this.strokeWidth = 3,
    this.paintingStyle = PaintingStyle.stroke,
  });

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = Colors.red
      ..strokeWidth = 2
      ..style = PaintingStyle.fill;

    canvas.drawPath(getTrianglePath(size.width, size.height), paint);
  }

  Path getTrianglePath(double x, double y) {
    return Path()
      ..moveTo(0, 0)
      ..lineTo(x - 1, 0)
      ..lineTo(x, 1)
      ..lineTo(x - 1, 3)
      ..lineTo(3, y - 1)
      ..lineTo(1, y)
      ..lineTo(0, y - 1)
      ..lineTo(0, 0);
  }

  @override
  bool shouldRepaint(SalePainter oldDelegate) {
    return oldDelegate.strokeColor != strokeColor ||
        oldDelegate.paintingStyle != paintingStyle ||
        oldDelegate.strokeWidth != strokeWidth;
  }
}

class PartnersItem extends StatelessWidget {
  final Function(PartnerModel) onClick;
  final PartnerModel model;

  PartnersItem({
    @required this.onClick,
    @required this.model,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onClick(model);
      },
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 13.5, left: 11),
                  child: Stack(
                    children: <Widget>[
                      Image.network(
                        BASE_URL_SHORT + model.img,
                        fit: BoxFit.fitWidth,
                        width: 150,
                        height: 75,
                      ),
                      Positioned(
                        child: CustomPaint(
                          painter: SalePainter(),
                          child: Container(
                            height: 37.0,
                            width: 50.5,
                          ),
                        ),
                      ),
                      Positioned(
                        top: 2.0,
                        left: 2.0,
                        child: Text(
                          '-${model.sale}%',
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            fontFamily: "Gotham",
                            fontWeight: FontWeight.w300,
                            fontSize: 13,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 11, top: 17, right: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          model.name,
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            fontFamily: "Gotham",
                            fontWeight: FontWeight.w300,
                            fontSize: 13,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          model.descShort == null
                              ? "{описание}"
                              : model.descShort,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            fontFamily: "Gotham",
                            fontWeight: FontWeight.w300,
                            fontSize: 13,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text(
                    CardUtil.categories(
                      categories: model.category,
                      defaultText: '{категория}',
                    ),
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      decoration: TextDecoration.none,
                      fontFamily: "Gotham",
                      fontWeight: FontWeight.w100,
                      fontSize: 13,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Container(
              height: 1,
              color: Colors.white,
              margin: EdgeInsets.only(left: 112),
            ),
          ],
        ),
      ),
    );
  }
}
