import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grad/web/webApi.dart';

class CategoriesItem extends StatelessWidget {
  final String text;
  final String image;
  final Alignment alignment;
  final Function onClick;

  CategoriesItem({
    @required this.text,
    @required this.image,
    @required this.alignment,
    @required this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClick,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            right: BorderSide(
              width: alignment == Alignment.topRight ||
                      alignment == Alignment.centerRight
                  ? 0
                  : 1,
              color: Colors.white,
            ),
            bottom: BorderSide(width: 1, color: Colors.white),
          ),
          color: Colors.transparent,
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 24),
            Image.network(
              BASE_URL_SHORT + image,
              fit: BoxFit.fitWidth,
              width: 70,
              height: 70,
            ),
            Container(
              height: 50,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    decoration: TextDecoration.none,
                    fontFamily: "Gotham",
                    fontWeight: FontWeight.w300,
                    fontSize: 12,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            SizedBox(height: 5),
          ],
        ),
      ),
    );
  }
}
