import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class CustomText extends Text {
  CustomText({
    @required this.textTitle,
    @required this.textSize,
    @required this.textWeight,
    this.textAlign
  }) : super(textTitle);

  final String textTitle;
  final double textSize;
  final FontWeight textWeight;
  final TextAlign textAlign;

  @override
  Widget build(BuildContext context) {
    return  Text(
        textTitle,
        textAlign: textAlign,
        style: TextStyle(
          decoration: TextDecoration.none,
          fontFamily: "Gotham",
          fontWeight: textWeight,
          color: Colors.white,
          fontSize: textSize
        )
    );
  }
}