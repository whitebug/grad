import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:grad/components/text.dart';

class CustomOutlineButton extends StatelessWidget {
  CustomOutlineButton({
    @required this.onPressed,
    @required this.titleText,
    @required this.textSize,
    @required this.textWeight
  });
  final GestureTapCallback onPressed;
  final String titleText;
  final double textSize;
  final FontWeight textWeight;

  @override
  Widget build(BuildContext context) {
    return OutlineButton(
      color: Colors.white,
      highlightColor: Colors.white,
      highlightedBorderColor: Colors.white,
      borderSide: BorderSide(color: Colors.white),
      shape: StadiumBorder(
          side: BorderSide(color: Colors.white)
      ),
      disabledBorderColor: Colors.grey,
      disabledTextColor: Colors.grey,
      child: CustomText(
        textTitle: titleText,
        textSize: textSize,
        textWeight: textWeight,
      ),
      onPressed: onPressed,
    );
  }
}