import 'webApi.dart';

const CATEGORIES_LIST_URL = "/noauth/categories";
const ORDER_HEADER_TOKEN = 'Auth-Token';

String getCategoriesUrl() => BASE_URL + CATEGORIES_LIST_URL;
String getCategoryUrl(int id) =>
    BASE_URL + CATEGORIES_LIST_URL + "/$id/partners?items=30&page=0";

Map<String, String> formHeader(String token) {
  Map<String, String> body = {ORDER_HEADER_TOKEN: '$token'};
  return body;
}

Map<String, String> formHeaderPartners(String token, int page) {
  Map<String, String> body = {
    ORDER_HEADER_TOKEN: '$token',
   
  };
  return body;
}

class CategoriesResponse {
  bool success;
  String message;
  List<CategoryModel> categories;

  CategoriesResponse({this.success, this.message, this.categories});
  factory CategoriesResponse.fromJson(Map<String, dynamic> json) {
    return CategoriesResponse(
      success: json['success'] as bool,
      message: json['message'] as String,
      categories: (json['data'] as List)
          .map((item) => CategoryModel.fromJson(item))
          .toList(),
    );
  }
}

class PartnersResponse {
  bool success;
  String message;
  List<PartnerModel> partners;

  PartnersResponse({
    this.success,
    this.message,
    this.partners,
  });
  factory PartnersResponse.fromJson(Map<String, dynamic> json) {
    return PartnersResponse(
      success: json['success'] as bool,
      message: json['message'] as String,
      partners: (json['data'] as List)
          .map((item) => PartnerModel.fromJson(item))
          .toList(),
    );
  }
}

class CategoryModel {
  int id;
  String imgLink;
  String name;

  CategoryModel({this.id, this.imgLink, this.name});
  factory CategoryModel.fromJson(Map<String, dynamic> json) {
    return CategoryModel(
      id: json['id'] as int,
      imgLink: json['img'] as String,
      name: json['name'] as String,
    );
  }
}

class PartnerModel {
  List<AddressModel> addresses;
  List<CategoryModel> category;
  int categoryId;
  String descFull;
  String descShort;
  String email;
  String facebook;
  int id;
  String img;
  String instagram;
  String link;
  String name;
  int orderNumber;
  List<PhoneModel> phones;
  String regDate;
  String uniqueCode;
  String vk;
  num sale;

  PartnerModel({
    this.addresses,
    this.category,
    this.categoryId,
    this.descFull,
    this.descShort,
    this.email,
    this.facebook,
    this.id,
    this.img,
    this.instagram,
    this.link,
    this.name,
    this.orderNumber,
    this.phones,
    this.regDate,
    this.uniqueCode,
    this.vk,
    this.sale,
  });
  factory PartnerModel.fromJson(Map<String, dynamic> json) {
    return PartnerModel(
      addresses: (json['addresses'] as List)
          .map((item) => AddressModel.fromJson(item))
          .toList(),
      category: [CategoryModel.fromJson(json['category']), CategoryModel.fromJson(json['category'])], ///TODO: map to list
      categoryId: json['categoryId'] as int,
      descFull: json['descFull'] as String,
      descShort: json['descShort'] as String,
      email: json['email'] as String,
      facebook: json['facebook'] as String,
      id: json['id'] as int,
      img: json['img'] as String,
      instagram: json['instagram'] as String,
      link: json['link'] as String,
      name: json['name'] as String,
      orderNumber: json['orderNumber'] as int,
      phones: (json['phones'] as List)
          .map((item) => PhoneModel.fromJson(item))
          .toList(),
      regDate: json['regDate'] as String,
      uniqueCode: json['uniqueCode'] as String,
      vk: json['vk'] as String,
      sale: 15, /// TODO: change to new API
    );
  }
}

class AddressModel {
  final String address;
  final int id;
  final double latitude;
  final double longitude;
  AddressModel({this.address, this.id, this.latitude, this.longitude});
  factory AddressModel.fromJson(Map<String, dynamic> json) {
    return AddressModel(
      address: json['address'] as String,
      id: json['id'] as int,
      latitude: json['latitude'] as double,
      longitude: json['longitude'] as double,
    );
  }
}

class PhoneModel {
  final int id;
  final String phone;
  PhoneModel({this.id, this.phone});
  factory PhoneModel.fromJson(Map<String, dynamic> json) {
    return PhoneModel(
      id: json['id'] as int,
      phone: json['phone'] as String,
    );
  }
}
