import 'dart:convert';
import 'webApi.dart';


const ORDER_URL = "/partner/order";
const ORDER_PARAM_USER_ID = 'customerId';
const ORDER_HEADER_TOKEN = 'Auth-Token';

String formQRMap(int customerId) {
  Map<String, int> body = {
    ORDER_PARAM_USER_ID: customerId
  };
  return jsonEncode(body);
}

Map<String, String> formHeader(String token) {
  Map<String, String> body = {
    ORDER_HEADER_TOKEN: '$token'
  };
  return body;
}

String getQRUrl() => BASE_URL + ORDER_URL;