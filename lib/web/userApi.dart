import 'dart:convert';
import 'webApi.dart';

//Запрос смс на номер телефона
const USER_CODE_REGISTRATION_URL = "/register/customer";
const USER_CODE_PARAM = 'phone';

String formUserPhoneMap(String phone) {
  Map<String, dynamic> body = {USER_CODE_PARAM: '$phone'};
  return jsonEncode(body);
}

String getUserRegistrationUrl() => BASE_URL + USER_CODE_REGISTRATION_URL;

class UserRegisterResponse {
  bool success;
  String message;
  UserRegistrationStatus data;

  UserRegisterResponse({this.success, this.message, this.data});
  factory UserRegisterResponse.fromJson(Map<String, dynamic> json) {
    return UserRegisterResponse(
        success: json['success'] as bool,
        message: json['message'] as String,
        data: UserRegistrationStatus.fromJson(json['data'])
    );
  }
}

class UserRegistrationStatus {
  bool registered;
  String code;

  UserRegistrationStatus({this.registered, this.code});
  factory UserRegistrationStatus.fromJson(Map<String, dynamic> json){
    return UserRegistrationStatus(
      registered: json['registered'] as bool,
      code: json['code'] as String,
    );
  }
}

//Отправка смс
const USER_SMS_CONFIRM_URL = "/register/customer/confirm";
const USER_SMS_CONFIRM_PARAM_CODE = 'code';
const USER_SMS_CONFIRM_PARAM_PHONE = 'phone';

String getUserSmsConfirmUrl() => BASE_URL + USER_SMS_CONFIRM_URL;


String formUserSmsMap( String code, String phone ) {
  Map<String, dynamic> body = {
    USER_SMS_CONFIRM_PARAM_CODE : '$code' ,
    USER_SMS_CONFIRM_PARAM_PHONE: '$phone'
  };
  return jsonEncode(body);
}


class UserSmsResponse {
  bool success;
  String message;
  UserInfo data;

  UserSmsResponse({this.success, this.message, this.data});
  factory UserSmsResponse.fromJson(Map<String, dynamic> json) {
    return UserSmsResponse(
        success: json['success'] as bool,
        message: json['message'] as String,
        data: UserInfo.fromJson(json['data'])
    );
  }
}


class UserInfo {
  int customerId;
  String createDate;
  String token;

  UserInfo({this.customerId, this.createDate, this.token});
  factory UserInfo.fromJson(Map<String, dynamic> json){
    return UserInfo(
      customerId: json['customerId'] as int,
      createDate: json['createDate'] as String,
      token: json['token'] as String
    );
  }
}

