import 'dart:convert';
import 'webApi.dart';

const PARTNER_CODE_REGISTRATION_URL = "/auth/partner";
const PARTNER_CODE_PARAM = 'uniqueCode';

const BECOME_A_PARTNER_URL = "/noauth/becomepartner";
const BECOME_A_PARTNER_NAME_PARAM = 'name';
const BECOME_A_PARTNER_PHONE_PARAM = 'phone';


String formPartnerCodeMap(String code) {
  Map<String, String> body = {PARTNER_CODE_PARAM: '$code'};
  return jsonEncode(body);
}

String getPartnerAuthUrl() => BASE_URL + PARTNER_CODE_REGISTRATION_URL;

class PartnerAuthResponse {
  bool success;
  String message;
  PartnerInfo data;

  PartnerAuthResponse({this.success, this.message, this.data});
  factory PartnerAuthResponse.fromJson(Map<String, dynamic> json) {
    return PartnerAuthResponse(
        success: json['success'] as bool,
        message: json['message'] as String,
        data: PartnerInfo.fromJson(json['data'])
    );
  }
}

class PartnerInfo {
  int partnerId;
  String createDate;
  String token;
  String partnerName;

  PartnerInfo({this.partnerId, this.createDate, this.token, this.partnerName});
  factory PartnerInfo.fromJson(Map<String, dynamic> json){
    return PartnerInfo(
        partnerId: json['partnerId'] as int,
        createDate: json['createDate'] as String,
        token: json['token'] as String,
        partnerName: json['partnerName'] as String
    );
  }
}


/**
 * Become a partner
 */

String getBecomeAPartnerUrl () => BASE_URL + BECOME_A_PARTNER_URL;


String formBecomeAPartnerMap ( String name, String phone) {
  Map<String, dynamic> body = {BECOME_A_PARTNER_NAME_PARAM : '$name', BECOME_A_PARTNER_PHONE_PARAM: '$phone'};
  return jsonEncode(body);
}

class BecomeAPartnerResponse {
  bool success;
  String message;
  dynamic data;

  BecomeAPartnerResponse({this.success, this.message, this.data});

  factory BecomeAPartnerResponse.fromJson(Map<String, dynamic> json) {
    return BecomeAPartnerResponse(
        success: json['success'] as bool,
        message: json['message'] as String,
        data: json['data']
    );
  }
}




