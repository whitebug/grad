import 'dart:convert';
import 'webApi.dart';

const HELP_DESC_URL = "/noauth/support";
const HELP_DESC_PARAM_EMAIL= 'name';
const HELP_DESC_PARAM_MESSAGE = 'message';
const HELP_DESC_PARAM_PHONE = 'phone';

String formHelpDescMap(String email, String phone, String message) {
  Map<String, dynamic> body = {
    HELP_DESC_PARAM_EMAIL: '$email',
    HELP_DESC_PARAM_MESSAGE: '$message',
    HELP_DESC_PARAM_PHONE: '$phone',
  };
  return jsonEncode(body);
}

String getHelpDescUrl() => BASE_URL + HELP_DESC_URL;

class HelpDescResponse {
  bool success;
  String message;
  dynamic data;

  HelpDescResponse({this.success, this.message, this.data});
  factory HelpDescResponse.fromJson(Map<String, dynamic> json) {
    return HelpDescResponse(
        success: json['success'] as bool,
        message: json['message'] as String,
        data: json['data']
    );
  }
}